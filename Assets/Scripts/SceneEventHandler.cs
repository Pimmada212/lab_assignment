using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Pimmada
{
    public class SceneEventHandler : MonoBehaviour
    {
        private UIDocument _uiDocument;

        private VisualElement _startButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _startButton = _uiDocument.rootVisualElement.Query<Button>("start-button");
        }

        private void OnEnable()
        {
            _startButton.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }

        private void OnStartButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("UIToolkit - Gameplay");
        }

        private void OnDisable()
        {
            _startButton.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }
    }
}
